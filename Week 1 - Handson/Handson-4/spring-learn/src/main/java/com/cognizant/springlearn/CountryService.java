package com.cognizant.springlearn;

import java.util.ArrayList;
import java.util.Iterator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CountryService {
	public static Country getCountry(String code) {
		ApplicationContext context = new ClassPathXmlApplicationContext("country.xml");
		ArrayList<Country> country = new ArrayList<Country>();
		country = context.getBean("countryList", ArrayList.class);
		int a = 1;
		for (Country cou : country) {
			if (cou.getCode().equalsIgnoreCase(code)) {
				a = 0;
				return cou;
			}
		}
		if (a == 1) {
			throw new CountryNotFoundException();
		}
		return null;
	}
}
