package com.cognizant.springlearn.controller;

import org.slf4j.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {

	@GetMapping("/hello")
	public String sayHello() {
		final Logger logger = LoggerFactory.getLogger(this.getClass());
		logger.info("Start");
		logger.debug("returning HELLO WORLD!!");
		logger.info("end");
		return "Hello World!!";
	}
}
